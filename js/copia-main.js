/*-------------INDICE--------------

1 - escribe en pantalla.
2 - calculation.
3 - ce borrar ultima linea.
4 - c borrar  todo.
5 - del flecha retroceder.

---------------------------------*/

/*----------------------------------------EXPLICACION-----------------------------------------

_____________________________________________________________________________________________________________________

1 - escribe en pantalla.

creamos la funcion print que se activa con los botones de los numeros
y define la funcion de imprimir el valor en la pantalla de abajo llamada entryNumber.
con dos variables operando son los numeros y operator son los operadores.
luego el primer if le decimos que si no hay numeros que escriba 
el valor que escribimos en pantalla, de no ser asi operando += significa que si ya 
hay numeros en pantalla que los añada detras de estos mismos el siguiente if
da la orden de mandar los numeros a la siguiente pantalla si apretamos un simbolo operator 
y la pantalla de abajo la deje limpia para poder seguir escribiendo numeros para la operacion

_____________________________________________________________________________________________________________________

_____________________________________________________________________________________________________________________

2 - calculation.

creamos la funcion calculation, que se activa con los operadores y establezco la variable fusion 
que significa que lo que hay en pantalla de arriba y abajo los junte, luego creamos la variable result 
y le decimos que es igual a "eval" que es un argumento interno de javascript. y lo que hace es
hacer una operacion matematica con los valores que hemos establecido en pantalla de arriba y abajo y luego le 
lo que calcula eval lo muestre en la pantalla de abajo dejando vacia la pantalla de arriba.

_____________________________________________________________________________________________________________________

_____________________________________________________________________________________________________________________

3 - ce borrar ultima linea.

creamos la funcion ce que se activara con el boton ce y le decimos que deje en blanco la pantalla de abajo

_____________________________________________________________________________________________________________________

_____________________________________________________________________________________________________________________

4 - c borrar  todo.

creamos la funcion c que se activara con el boton c y le decimos que deje en blanco todas las pantallas .

_____________________________________________________________________________________________________________________

5 - del flecha retroceder.

establezco la variable operando y le digo que es igual a lo que hay escrito en la pantalla de abajo,
y establecemos la variable back y le decimos que es igual a todo lo que hay escrito y con el -1 le decimos
que borre la ultima posicion de lo que hay en pantalla


substring es una funcion de javascript. 

operando.length es un atributo de operando interno de javascript. 

_____________________________________________________________________________________________________________________

_____________________________________________________________________________________________________________________

6 - evitar repeticion del 0


si lo que estoy escriendo es 0 y ya hay un puto 0
lo que hay le quitaremos lo último
y esta modificación se pondrá en pantalla

si escribo 0 y ya hay 0 borrame el ultimo 0 escrito

_____________________________________________________________________________________________________________________

/* 1 - escribe en pantalla */
/* value es lo que e pulsado */
// declaramos funcion print que recibira un parametro valor
function print(value){

                     //esto llama al codigo html
    var operando =  document.getElementById("entryNumber").innerHTML; 
                    //esto es un array
    var operator = [ '+' , '-' , '*' , '/']; 

    if(operando == ''){ 
        document.getElementById("entryNumber").innerHTML= value;
    }else{ 
    
        operando += value;
        document.getElementById("entryNumber").innerHTML= operando;    
    }

    if(operator.includes(value)){

        document.getElementById("exitNumber").innerHTML= operando;    
        document.getElementById("entryNumber").innerHTML= '';
        
    }
}

/* 2 - calculation */
function calculation(){

    var fusion = document.getElementById("exitNumber").innerHTML + document.getElementById("entryNumber").innerHTML;
    var result = eval(fusion);
    document.getElementById("entryNumber").innerHTML = result;
    document.getElementById("exitNumber").innerHTML = '';
}
/* 3 - ce borrar ultima linea */
function ce(){

    document.getElementById("entryNumber").innerHTML = '';
}
/* 4 - c borra todo */
function c(){

    document.getElementById("exitNumber").innerHTML = '';
    document.getElementById("entryNumber").innerHTML = '';
}

/* 5 - del flecha retroceder */
function del()  { 

 var operando = document.getElementById("entryNumber").innerHTML;

 var back = operando.substring(0, operando.length - 1);
 document.getElementById("entryNumber").innerHTML = operando;


}
/* 6 - evitar repeticion del 0 */